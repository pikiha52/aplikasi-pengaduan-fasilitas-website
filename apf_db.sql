-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Nov 03, 2020 at 02:27 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `apf_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `apf_access_menu`
--

CREATE TABLE `apf_access_menu` (
  `id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `apf_access_menu`
--

INSERT INTO `apf_access_menu` (`id`, `role_id`, `menu_id`) VALUES
(1, 1, 1),
(2, 1, 3),
(3, 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `apf_menu`
--

CREATE TABLE `apf_menu` (
  `id` int(11) NOT NULL,
  `menu` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `apf_menu`
--

INSERT INTO `apf_menu` (`id`, `menu`) VALUES
(1, 'Admin'),
(2, 'User'),
(3, 'Menu');

-- --------------------------------------------------------

--
-- Table structure for table `apf_sub_menu`
--

CREATE TABLE `apf_sub_menu` (
  `id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `url` varchar(100) NOT NULL,
  `icon` varchar(100) NOT NULL,
  `is_active` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `apf_sub_menu`
--

INSERT INTO `apf_sub_menu` (`id`, `menu_id`, `title`, `url`, `icon`, `is_active`) VALUES
(1, 1, 'Daftar Pengaduan Admin', 'apf-c-admin/admin', 'mdi mdi-gauge link-icon', 1),
(2, 2, 'Pengaduan ', 'apf-c-user/user', 'mdi mdi-clipboard-text', 1),
(3, 2, 'Pengaduan Saya', 'apf-c-user/pengaduan_saya', 'mdi mdi-clipboard-account', 0),
(4, 3, 'Menu Management', 'apf-c-admin/menu', 'mdi mdi-laptop-chromebook', 0),
(5, 1, 'Pesan', 'apf-c-admin/pesan', 'mdi mdi-inbox', 0);

-- --------------------------------------------------------

--
-- Table structure for table `pengaduan`
--

CREATE TABLE `pengaduan` (
  `id_pengaduan` int(11) NOT NULL,
  `nama_fasilitas` varchar(50) NOT NULL,
  `tempat_fasilitas` varchar(50) NOT NULL,
  `kondisi_fasilitas` varchar(50) NOT NULL,
  `lokasi_fasilitas` varchar(50) NOT NULL,
  `isi_laporan` varchar(300) NOT NULL,
  `waktu_pengaduan` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `status_pengaduan` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pengaduan`
--

INSERT INTO `pengaduan` (`id_pengaduan`, `nama_fasilitas`, `tempat_fasilitas`, `kondisi_fasilitas`, `lokasi_fasilitas`, `isi_laporan`, `waktu_pengaduan`, `status_pengaduan`) VALUES
(14, 'Universitas Brawijaya', 'Kamar mandi laki-laki lt.2', 'Pintu tidak bisa ditutup', 'Universitas Brawijaya ', 'Contoh..', '2020-11-03 13:11:51', 'Perlu Tindakan');

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE `role` (
  `id` int(11) NOT NULL,
  `role` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`id`, `role`) VALUES
(1, 'Administrator'),
(2, 'Member');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL,
  `nim` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `foto` varchar(50) NOT NULL,
  `role_id` int(11) NOT NULL,
  `is_active` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `nim`, `password`, `nama`, `foto`, `role_id`, `is_active`) VALUES
(3, '175150200111050', '$2y$10$7bBy4GQAOySO5HLPRNDWF.YprTK4a15zntMHhoT.qHRbAxYKTwm1q', 'Akhmad Dimitri Baihaqi', 'default.png', 2, 1),
(4, '175150201111048', '$2y$10$OBLU5H7cV4ENXVAnztNH3eQJrQmcDybjF3rfGFktTEtfgciJ6QBQy', 'Afgani Fajar Rizky', 'default.png', 2, 1),
(5, '175150200111038', '$2y$10$0JWbyv2quEcawjhIbXzCo.ajIe6Xmgt2LaV10.uX5jNPFpnLQpoyG', 'Niluh Putu Vania Dyah Saraswati', 'default.png', 1, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `apf_access_menu`
--
ALTER TABLE `apf_access_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `apf_menu`
--
ALTER TABLE `apf_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `apf_sub_menu`
--
ALTER TABLE `apf_sub_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pengaduan`
--
ALTER TABLE `pengaduan`
  ADD PRIMARY KEY (`id_pengaduan`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `apf_access_menu`
--
ALTER TABLE `apf_access_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `apf_menu`
--
ALTER TABLE `apf_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `apf_sub_menu`
--
ALTER TABLE `apf_sub_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `pengaduan`
--
ALTER TABLE `pengaduan`
  MODIFY `id_pengaduan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `role`
--
ALTER TABLE `role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
