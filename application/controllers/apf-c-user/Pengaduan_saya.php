<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengaduan_saya extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_pengaduan');
      }

    public function index(){

        $data['user'] = $this->db->get_where('user', ['nim' => $this->session->userdata('nim')])->row_array();
        $data['M_pengaduan'] = $this->M_pengaduan->all();
        $data['title'] = 'APF - Aplikasi Pengaduan Fakulitas - Pengaduan Saya';
        $data['page'] = 'Pengaduan Saya';


        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar');
        $this->load->view('apf-v-user/pengaduan-saya', $data);
        $this->load->view('templates/footer', $data);
    }

}