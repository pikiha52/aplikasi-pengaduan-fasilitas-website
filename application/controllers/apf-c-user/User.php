<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

    public function index(){

        $this->form_validation->set_rules('nama_fasilitas', 'Nama_Fasilitas', 'required');
        $this->form_validation->set_rules('tempat_fasilitas', 'Tempat_fasilitas', 'required');
        $this->form_validation->set_rules('kondisi_fasilitas','Kondisi_fasilitas', 'required');
        $this->form_validation->set_rules('lokasi_fasilitas','Lokasi_fasilitas', 'required');
        $this->form_validation->set_rules('isi_laporan', 'Isi_laporan', 'required');
        if ($this->form_validation->run() == false) {
        $data['user'] = $this->db->get_where('user', ['nim' => $this->session->userdata('nim')])->row_array();
        $data['title'] = 'APF - Aplikasi Pengaduan Fasilitas';
        $data['footer'] = 'APF - Aplikasi Pengaduan Fasilitas';

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar');
        $this->load->view('apf-v-user/index');
        $this->load->view('templates/footer', $data); } else {
            $data = [
                'nama_fasilitas' => htmlspecialchars($this->input->post('nama_fasilitas', true)),
                'tempat_fasilitas' => htmlspecialchars($this->input->post('tempat_fasilitas', true)),
                'kondisi_fasilitas' => htmlspecialchars($this->input->post('kondisi_fasilitas', true)),
                'lokasi_fasilitas' => htmlspecialchars($this->input->post('lokasi_fasilitas', true)),
                'isi_laporan' => htmlspecialchars($this->input->post('isi_laporan', true)),
                'status_pengaduan' => 'Perlu Tindakan'
            ];
    
            $this->db->insert('pengaduan', $data);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            <h4 class="alert-heading">Well done!</h4>
            <p>Pengaduan telah dikirim ke admin. Terimakasih</p>
            <hr>
          </div>');
            redirect('apf-c-user/user');
        }
    }
}