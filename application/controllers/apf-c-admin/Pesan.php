<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pesan extends CI_Controller {

    public function index(){
        $data['user'] = $this->db->get_where('user', ['nim' => $this->session->userdata('nim')])->row_array();
        $data['title'] = 'APF - Aplikasi Pengaduan Fasilitas';
        $data['page'] = 'APF - Daftar Pengaduan Fasilitas';

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar');
        $this->load->view('apf-v-admin/chat', $data);
        $this->load->view('templates/footer', $data);
    }
}