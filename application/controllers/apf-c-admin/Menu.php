<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Menu extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('M_menu');
    }

	public function index() {
        
        $data['user']=$this->db->get_where('user', ['nim'=> $this->session->userdata('nim')])->row_array();
		$data['title']='APF - Aplikasi Pengaduan Fasilitas - Menu Management';
		$data['page']='Menu Management';

		$data['menu']=$this->db->get('apf_menu')->result_array();

        $this->form_validation->set_rules('menu', 'Menu', 'required');

        if ($this->form_validation->run() == false) {
            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar');
            $this->load->view('apf-v-admin/menu/index', $data);
            $this->load->view('templates/footer', $data);
        } else {
            $this->db->insert('apf_menu', ['menu' => $this->input->post('menu')]);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Menu baru berhasil ditambahkan!</div>');
            redirect('apf-c-admin/menu');   
        }
    }
    
    public function delete($id)
    {
        $this->M_pengaduan->delete($id);
        $this->session->set_flashdata('pesan', '<script>alert("Data Berhasil DIhapus")</script>');
        redirect(base_url('apf-c-admin/menu'));
    }
}
