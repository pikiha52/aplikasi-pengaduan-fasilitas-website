<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_pengaduan');
      }

    public function index(){
        $data['user'] = $this->db->get_where('user', ['nim' => $this->session->userdata('nim')])->row_array();
        $data['proses'] = $this->M_pengaduan->proses_tampil();
        $data['title'] = 'APF - Aplikasi Pengaduan Fasilitas - Daftar Pengaduan Fasilitas';
        $data['page'] = 'APF - Daftar Pengaduan Fasilitas';

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar');
        $this->load->view('apf-v-admin/index', $data);
        $this->load->view('templates/footer', $data);
    }

    public function detail_pengaduan($id){
        
        $data['user'] = $this->db->get_where('user', ['nim' => $this->session->userdata('nim')])->row_array();
        $detail = $this->M_pengaduan->get_detail($id);
        $data['detail'] = $detail;
        $data['title'] = 'APF - Aplikasi Pengaduan Fasilitas - Detail Pengaduan Fasilitas';
        $data['page'] = 'APF - Daftar Pengaduan Fasilitas';

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar');
        $this->load->view('apf-v-admin/detail_pengaduan', $data);
        $this->load->view('templates/footer', $data);
    }

    public function delete($id)
{
    $this->M_pengaduan->delete($id);
    $this->session->set_flashdata('pesan', '<script>alert("Pengaduan Berhasil DIhapus")</script>');
    redirect(base_url('apf-c-admin/Admin'));
}

}