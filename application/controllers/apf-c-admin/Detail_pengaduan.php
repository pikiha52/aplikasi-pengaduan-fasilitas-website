<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Detail_pengaduan extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('M_pengaduan');
        $this->load->model('M_edit');
      }

    
    public function index ($id){
      $data=array(
        "all"=>$this->db->get('pengaduan')->result(),
    );
        
        $data['user'] = $this->db->get_where('user', ['nim' => $this->session->userdata('nim')])->row_array();
        $detail = $this->M_pengaduan->get_detail($id);
        $data['detail'] = $detail;
        $data['title'] = 'APF - Aplikasi Pengaduan Fasilitas - Detail Pengaduan Fasilitas';
       

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar');
        $this->load->view('apf-v-admin/detail_pengaduan', $data);
        $this->load->view('templates/footer', $data);
    }

    private function rules()
    {
        return[
            ['field' => 'status_pengaduan', 'label' => 'Status_pengaduan', 'rules' => 'required']
        ];
    }

    function edit_status($id)
    {
        $this->form_validation->set_rules($this->rules());

        if($this->form_validation->run() == FALSE) {
            $data['title'] = 'APF - Aplikasi Pengaduan Fasilitas - Edit Status Laporan';
            $data["edit"]= $this->M_edit->getId($id);
            $data['user'] = $this->db->get_where('user', ['nim' => $this->session->userdata('nim')])->row_array();     
            // $data['title'] = 'Edit';
            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar');
            $this->load->view('apf-v-admin/edit', $data);
            $this->load->view('templates/footer', $data);
        } else {
            // $data['username'] = $this->input->post('username');
            // $data['full_name'] = $this->input->post('full_name');
            // $data['email'] = $this->input->post('email');
            $data['status_pengaduan'] = $this->input->post('status_pengaduan');

            $this->M_edit->edit($id, $data);
            $this->session->set_flashdata('pesan', '<script>alert("Data Berhasil Diubah")</script>');

            redirect(base_url('apf-c-admin/admin'));
        }
    }

    function delete(){
      $hapus_pengaduan=$this->input->post('hapus_pengaduan');
      $this->M_pengaduan->delete($hapus_pengaduan);
      redirect('apf-c-admin/Admin');
  }

  function edit(){
    $id = $this->input->post('id');
       $data = array(
           'status_pengaduan' => $this->input->post('status_pengaduan')
       );
       $this->M_pengaduan->ubah($data,$id);
       $this->session->set_flashdata('notif','<div class="alert alert-success" role="alert"> Data Berhasil diubah <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
       redirect('apf-c-admin/admin');
   }

    }