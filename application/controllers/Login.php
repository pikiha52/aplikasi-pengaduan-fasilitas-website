<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
    }


    public function index()
    {   
        $data['title'] = 'APF - Aplikasi Pengaduan Fasilitas';
        
        $this->form_validation->set_rules('nim', 'Nim', 'trim|required');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');
        if ($this->form_validation->run() == false) {

        $this->load->view("apf-login/login", $data);
    } else {
        //validation succes
        $this->_index();
    }
}
private function _index()
{
    $nim = $this->input->post('nim');
    $password = $this->input->post('password');

    $user = $this->db->get_where('user', ['nim' => $nim])->row_array();

    //jika usernya ada
    if ($user) {
        //jika usenya aktif
        if ($user['is_active'] == 1) {
            //cek password
            if (password_verify($password, $user['password'])) {
                $data = [
                    'user_id' => $user['user_id'],
                    'nim' => $user['nim'],
                    'role_id' => $user['role_id']
                ];
                $this->session->set_userdata($data);
                if ($user['role_id'] == 1) {
                    redirect('apf-c-admin/admin');
                } else {
                    redirect('apf-c-user/user');
                     }
                        $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">role id  null!!</div>');
                        redirect('login');        
                     
                    }else{
                        $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Password yang anda masukan salah!!</div>');
                        redirect('login');
                    }
        } 
    } else {
        $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Nim yang anda masukan salah!</div>');
        redirect('login');
    }
}

public function register()
{
    $data['title'] ='Please Register';

    $this->form_validation->set_rules('password', 'Password', 'required|trim');
    if ($this->form_validation->run() == false) {
        $this->load->view('apf-login/register', $data);
    } else {
        $data = [
			'nim' => htmlspecialchars($this->input->post('nim', true)),
			'password' => password_hash($this->input->post('password'), PASSWORD_DEFAULT),
            'nama' => htmlspecialchars($this->input->post('nama')),
            'role_id' => '2',
            'foto' => 'default.png',
            'is_active' => 1
        ];

        $this->db->insert('user', $data);
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Congratulation! your account has been created. Please 
        </div>');
        redirect('login');
    }
}

    public function logout()
    {
        $this->session->unset_userdata('nim');
        $this->session->unset_userdata('role_id');
        $this->session->set_flashdata('message', '<div class="alert alert-succes" role="alert">Anda Telah Berhasil Keluar !</div>');
        redirect('login');
    }

}
