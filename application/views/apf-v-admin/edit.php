<div class="page-content-wrapper">
	<div class="page-content-wrapper-inner">
		<div class="viewport-header">
			<?= $this->session->flashdata('message'); ?>
			<div class="col-lg-12">
				<div class="grid">
                <form role="form" action="<?php echo base_url('apf-c-admin/detail_pengaduan/edit_status/' .$edit[0]->id_pengaduan); ?>" method="post">
					<div class="row showcase_row_area">
						<div class="col-md-3 showcase_text_area">
							<label>Status Pengaduan</label>
						</div>
						<div class="col-md-9 showcase_content_area">
							<select class="custom-select" name="status_pengaduan">
                            <?php echo $edit[0]->status_pengaduan?>
                                <option value="">-</option>
                                <option value="Perlu Tindakan">Perlu Tindakan</option>
                                <option value="Sedang Proses">Sedang Proses</option>
                                <option value="Selesai">Selesai</option>
							</select>
						</div>
                    </div>
                    <center>
					<button type="submit" class="btn btn-primary">Simpan</button>
					<a href="<?php echo base_url('apf-c-admin/admin') ?>" class="btn btn-primary">Batal</a>
                    </center>
                </form>
				</div>
			</div>
		</div>
	</div>
</div>
