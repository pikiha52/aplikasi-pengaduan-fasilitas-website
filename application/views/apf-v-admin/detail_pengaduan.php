<div class="page-content-wrapper">
	<div class="page-content-wrapper-inner">
		<div class="viewport-header">
			<?= $this->session->flashdata('message'); ?>
			<div class="col-lg-12">
				<div class="grid">

					<div class="grid-body">
						<div class="item-wrapper">
							<div class="row mb-3">
								<div class="col-md-8 mx-auto">
									<div class="form-group row showcase_row_area">
										<div class="col-md-3 showcase_text_area">
											<label for="inputType1">Nama Fasilitas</label>
										</div>
										<div class="col-md-9 showcase_content_area">
											<input type="text" class="form-control" id="inputType1" readonly
												value="<?php echo $detail->nama_fasilitas ?>">
										</div>
									</div>
									<div class="form-group row showcase_row_area">
										<div class="col-md-3 showcase_text_area">
											<label for="inputType12">Tempat Fasilitas</label>
										</div>
										<div class="col-md-9 showcase_content_area">
											<input type="text" class="form-control" id="inputType2" readonly
												value="<?php echo $detail->tempat_fasilitas ?>">
										</div>
									</div>
									<div class="form-group row showcase_row_area">
										<div class="col-md-3 showcase_text_area">
											<label for="inputType13">Kondisi Fasilitas</label>
										</div>
										<div class="col-md-9 showcase_content_area">
											<input type="text" class="form-control" id="inputType3" readonly
												value="<?php echo $detail->kondisi_fasilitas ?>"></div>
									</div>
									<div class="form-group row showcase_row_area">
										<div class="col-md-3 showcase_text_area">
											<label for="inputType13">Lokasi Fasilitas</label>
										</div>
										<div class="col-md-9 showcase_content_area">
											<input type="text" class="form-control" id="inputType3" readonly
												value="<?php echo $detail->lokasi_fasilitas ?>"> </div>
									</div>
									<div class="form-group row showcase_row_area">
										<div class="col-md-3 showcase_text_area">
											<label for="inputType13">Waktu Laporan Dibuat</label>
										</div>
										<div class="col-md-9 showcase_content_area">
											<input type="text" class="form-control" id="inputType3" readonly
												value="<?php echo $detail->waktu_pengaduan ?>"> </div>
									</div>
									<div class="row showcase_row_area">
										<div class="col-md-3 showcase_text_area">
											<label>Status Pengaduan</label>
										</div>
										<div class="col-md-9 showcase_content_area">
											<select class="custom-select">
                        <option value="1"><?php echo $detail->status_pengaduan?></option>
											</select>
											<a href="<?php echo base_url('apf-c-admin/detail_pengaduan/edit_status/'.$detail->id_pengaduan) ?>"
												class="btn btn-rounded social-icon-btn btn-pinterest"><i class="mdi mdi-grease-pencil"></i></a>
										</div>
									</div>
									<div class="form-group row showcase_row_area">
										<div class="col-md-3 showcase_text_area">
											<label for="inputType9">Isi Laporan</label>
										</div>
										<div class="col-md-9 showcase_content_area">
											<textarea class="form-control" id="inputType9" cols="12" rows="5" name="isi_laporan"
												readonly> <?php echo $detail->isi_laporan ?></textarea>
										</div>
									</div>


									<center>
										<div class="tooltip-demo">
											<a href="<?php echo site_url('apf-c-admin/Admin/delete/'.$detail->id_pengaduan); ?>"
												onclick="return confirm('Apakah Anda Ingin Menghapus Data <?=$detail->kondisi_fasilitas;?> ?');"
												class="btn btn-danger btn-circle" data-popup="tooltip" data-placement="top"
												title="Hapus Data"><i class="mdi mdi-archive"></i></a>
										</div>
									</center>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			</form>
		</div>
	</div>
</div>

