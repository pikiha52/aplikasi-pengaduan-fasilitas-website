<div class="page-content-wrapper">
        <div class="page-content-wrapper-inner">
          <div class="content-viewport">
            <div class="row">
              <div class="col-12 py-5">
                <h4>Dashboard</h4>
                <p class="text-gray">Selamat datang, <?= $user['nama']; ?></p>
              </div>
            </div>
            <div class="row">
              <div class="col-md-8 equel-grid">
                <div class="grid">
                <?= $this->session->flashdata('message'); ?>
                  <div class="grid-body py-3">
                    <p class="card-title ml-n1">Daftar Pengaduan Fasilitas</p>
                  </div>
                  <div class="table-responsive">
                    <table class="table table-hover table-sm">
                      <thead>
                        <tr class="solid-header">
                          <th>#</th>
                          <th>Nama Fasilitas</th>
                          <th>Tempat Fasilitas</th>
                          <th>Isi Laporan</th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php $i = 1; ?>
                      <?php foreach ($proses->result() as $item) : ?>
                        <tr>
                          <td><?= $i; ?></td>
                         <td><?php echo $item->nama_fasilitas ?></td>
                          <td>
                            <small><?php echo $item->tempat_fasilitas ?></small>
                          </td>
                          <td><?php echo $item->isi_laporan ?></td>
                        <td><a href="<?php echo site_url('apf-c-admin/admin/detail_pengaduan/' .$item->id_pengaduan)?>" class="btn btn-success has-icon btn-rounded mb-3">Detail Laporan</a></td>
                        </tr>
                        <?php $i++; ?>
                        <?php endforeach; ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            
            </div>
          </div>
        </div>

          <!-- Start of LiveChat (www.livechatinc.com) code -->
<script>
    window.__lc = window.__lc || {};
    window.__lc.license = 12330594;
    ;(function(n,t,c){function i(n){return e._h?e._h.apply(null,n):e._q.push(n)}var e={_q:[],_h:null,_v:"2.0",on:function(){i(["on",c.call(arguments)])},once:function(){i(["once",c.call(arguments)])},off:function(){i(["off",c.call(arguments)])},get:function(){if(!e._h)throw new Error("[LiveChatWidget] You can't use getters before load.");return i(["get",c.call(arguments)])},call:function(){i(["call",c.call(arguments)])},init:function(){var n=t.createElement("script");n.async=!0,n.type="text/javascript",n.src="https://cdn.livechatinc.com/tracking.js",t.head.appendChild(n)}};!n.__lc.asyncInit&&e.init(),n.LiveChatWidget=n.LiveChatWidget||e}(window,document,[].slice))
</script>
<noscript><a href="https://www.livechatinc.com/chat-with/12330594/" rel="nofollow">Chat with us</a>, powered by <a href="https://www.livechatinc.com/?welcome" rel="noopener nofollow" target="_blank">LiveChat</a></noscript>
<!-- End of LiveChat code -->
        <!-- content viewport ends -->