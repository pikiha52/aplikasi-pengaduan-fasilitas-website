<div class="page-content-wrapper">
        <div class="page-content-wrapper-inner">
          <div class="viewport-header">
          <?= $this->session->flashdata('message'); ?>
          <form class="contact100-form validate-form" method="post" action="<?= base_url('apf-c-user/User'); ?>">
<div class="col-lg-12">
                <div class="grid">
                  
                  <div class="grid-body">
                    <div class="item-wrapper">
                      <div class="row mb-3">
                        <div class="col-md-8 mx-auto">
                          <div class="form-group row showcase_row_area">
                            <div class="col-md-3 showcase_text_area">
                              <label for="inputType1">Nama Fasilitas</label>
                            </div>
                            <div class="col-md-9 showcase_content_area">
                              <input type="text" class="form-control" id="inputType1" name="nama_fasilitas">
                            </div>
                          </div>
                          <div class="form-group row showcase_row_area">
                            <div class="col-md-3 showcase_text_area">
                              <label for="inputType12">Tempat Fasilitas</label>
                            </div>
                            <div class="col-md-9 showcase_content_area">
                              <input type="text" class="form-control" id="inputType2" name="tempat_fasilitas"">
                            </div>
                          </div>
                          <div class="form-group row showcase_row_area">
                            <div class="col-md-3 showcase_text_area">
                              <label for="inputType13">Kondisi Fasilitas</label>
                            </div>
                            <div class="col-md-9 showcase_content_area">
                              <input type="text" class="form-control" id="inputType3" name="kondisi_fasilitas""> </div>
                          </div>
                          <div class="form-group row showcase_row_area">
                            <div class="col-md-3 showcase_text_area">
                              <label for="inputType13">Lokasi Fasilitas</label>
                            </div>
                            <div class="col-md-9 showcase_content_area">
                              <input type="text" class="form-control" id="inputType3" name="lokasi_fasilitas""> </div>
                          </div>
                          <div class="form-group row showcase_row_area">
                            <div class="col-md-3 showcase_text_area">
                              <label for="inputType9">Isi Laporan</label>
                            </div>
                            <div class="col-md-9 showcase_content_area">
                              <textarea class="form-control" id="inputType9" cols="12" rows="5" name="isi_laporan"></textarea>
                            </div>
                          </div>
                          <button type="submit" class="btn btn-sm btn-primary">Buat Laporan</button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
</form>
          </div>
        </div>
</div>