<div class="page-body">
      <!-- partial:partials/_sidebar.html -->
      <div class="sidebar">
        <div class="user-profile">
          <div class="display-avatar animated-avatar">
            <img class="profile-img img-lg rounded-circle" src="<?= base_url('assets/images/profile/') . $user['foto']; ?>" alt="profile image">
          </div>
          <div class="info-wrapper">
            <p class="user-name"><?= $user['nama']; ?></p>
            <h6 class="display-income"><?= $user['nim']; ?></h6>
          </div>
        </div>
        <ul class="navigation-menu">
        <?php 
            $role_id = $this->session->userdata('role_id');
            $queryMenu = "SELECT `apf_menu`.`id`, `menu`
                            FROM `apf_menu` JOIN `apf_access_menu`
                              ON `apf_menu`.`id` = `apf_access_menu`.`menu_id`
                           WHERE `apf_access_menu`.`role_id` = $role_id
                        ORDER BY `apf_access_menu`.`menu_id` ASC
                        ";
            $menu = $this->db->query($queryMenu)->result_array();
            ?>

<?php foreach ($menu as $m) : ?>
          <li class="nav-category-divider"> <?= $m['menu']; ?></li>

          <li>
          <?php 
            $menuId = $m['id'];
            $querySubMenu = "SELECT *
                               FROM `apf_sub_menu` JOIN `apf_menu` 
                                 ON `apf_sub_menu`.`menu_id` = `apf_menu`.`id`
                              WHERE `apf_sub_menu`.`menu_id` = $menuId
                              AND `apf_sub_menu`.`is_active` = 1
                        ";
            $subMenu = $this->db->query($querySubMenu)->result_array();
            ?>

<?php foreach ($subMenu as $sm) : ?>
            <a href="<?= base_url($sm['url']); ?>">
              <span class="link-title"><?= $sm['title']; ?></span>
              <i class="<i class="<?= $sm['icon']; ?>"></i>
              <?php endforeach; ?>
            </a>
          </li>
          <?php endforeach; ?>
        </ul>
      </div>
      <!-- partial -->