        <!-- partial:partials/_footer.html -->
        <footer class="footer">
          <div class="row">
            <div class="col-sm-6 text-center text-sm-left mt-3 mt-sm-0">
              <small class="text-muted d-block">Copyright © APF <a href="#" target="_blank"></a>. All rights reserved</small>
              <small class="text-gray mt-2"><?=$title?> <i class="mdi mdi-bookmark-check"></i></small>
            </div>
          </div>
        </footer>
        <!-- partial -->
      </div>
      <!-- page content ends -->
    </div>

    <!--page body ends -->
    <!-- SCRIPT LOADING START FORM HERE /////////////-->
    <!-- plugins:js -->
    <script src="<?php echo base_url()?>assets/vendors/js/core.js"></script>
    <!-- endinject -->
    <!-- Vendor Js For This Page Ends-->
    <script src="<?php echo base_url()?>assets/vendors/apexcharts/apexcharts.min.js"></script>
    <script src="<?php echo base_url()?>assets/vendors/chartjs/Chart.min.js"></script>
    <script src="<?php echo base_url()?>assets/js/charts/chartjs.addon.js"></script>
    <!-- Vendor Js For This Page Ends-->
    <!-- build:js -->
    <script src="<?php echo base_url()?>assets/js/template.js"></script>
    <script src="<?php echo base_url()?>assets/js/dashboard.js"></script>
    <!-- endbuild -->
  </body>
</html>