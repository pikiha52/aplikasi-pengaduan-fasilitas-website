<?php
class M_edit extends CI_Model{
private $table = 'pengaduan';
private $primary_key = 'id_pengaduan';
   function selectAll()
   {
		$this->db->order_by("id_pengaduan","asc"); 
		return $this->db->get('pengaduan')->result();
   }
   public function getId($id)
{
   return $this->db->get_where($this->table, array($this->primary_key => $id))->result();
}

public function insert($data)
{
   $this->db->insert($this->table, $data);
}

public function edit($id_pengaduan, $data)
{
   $this->db->update($this->table, $data, array($this->primary_key => $id_pengaduan));
}

public function delete($id)
{
$this->db->delete($this->table, array($this->primary_key => $id));
}
public function saveProduct($data){
   $query = $this->db->table('upload')->insert($data);
   return $query;
}
}
?>