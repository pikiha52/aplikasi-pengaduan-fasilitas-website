<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class M_pengaduan extends CI_Model{
private $table = 'pengaduan';
private $primary_key = 'id_pengaduan';
    public function all()
    {
        $hasil = $this->db->get('pengaduan');
        if($hasil->num_rows() > 0){
            return $hasil->result();
        } else {
            return array();
        }
    }

    public function getId($id)
{
   return $this->db->get_where($this->table, array($this->primary_key => $id))->result();
}

 public function proses_tampil(){
     $sql = $this->db->get('pengaduan');
     return $sql;
 }

 public function get_detail($id = NULL)
 {
     $query = $this->db->get_where('pengaduan', array('id_pengaduan' => $id))->row();
     return $query;
 }

public function edit($id_pengaduan, $data)
{
   $this->db->update($this->table, $data, array($this->primary_key => $id_pengaduan));
}

public function insert($data)
{
   $this->db->insert($this->table, $data);
}

public function delete($id)
{
$this->db->delete($this->table, array($this->primary_key => $id));
}

    public function search($keyword)
    {
        $this->db->like('nama', $keyword);
        $this->db->or_like('harga', $keyword);

        $result = $this->db->get('product')->result();
        return $result;
    }
}